import numpy as np
from solver import Solver
import sys, getopt, time
from pGUI import *

#samples
#goal_state = np.array([[1, 2, 3],
#                       [4, 5, 6],
#                       [7, 8, 0]])
#
#init_state = np.array([[1, 8, 7],
#                       [3, 0, 5],
#                       [4, 6, 2]])

def start(init_state, goal_state, max_iter, evalf, algo):
    solver = Solver(init_state, goal_state, algo, evalf, max_iter)
    path = solver.solve()

    initializeGUI() #Initialize pygame and GUI configs
    time.sleep(0.5)
    if len(path) == 0:
        checkForQuit()
        #exit(1)

    init_idx = init_state.flatten().tolist().index(0) #Index of the Zero(Blank) Tile acc to the flattened list
    init_i, init_j = init_idx // goal_state.shape[0], init_idx % goal_state.shape[0] #Index acc to the ndarray

    print()
    print('INITIAL STATE')
    for i in range(goal_state.shape[0]):
        print(init_state[i, :])
    print()
    time.sleep(0.5)

    # **************pass the init_state(which is a numpy ndarray) to the pGUI fns to draw it************
    drawBoard(init_state.transpose())
    pygame.display.update()
    pygame.time.wait(1000) # pause 500 milliseconds for effect


    prevState = init_state
    for node in reversed(path): #Reversed path to get the order of nodes from Initial state to the Goal state

        # *************Declare a variable to save the move done from prev to next node state***************
        move = None

        cur_idx = node.get_state().index(0) #Index of the Zero(Blank) Tile acc to the flattened list of the node game state
        cur_i, cur_j = cur_idx // goal_state.shape[0], cur_idx % goal_state.shape[0]

        new_i, new_j = cur_i - init_i, cur_j - init_j
        if new_j == 0 and new_i == -1:
            move = 'down'
            print('Moved UP    from ' + str((init_i, init_j)) + ' --> ' + str((cur_i, cur_j)))
        elif new_j == 0 and new_i == 1:
            move = 'up'
            print('Moved DOWN  from ' + str((init_i, init_j)) + ' --> ' + str((cur_i, cur_j)))
        elif new_i == 0 and new_j == 1:
            move = 'left'
            print('Moved RIGHT from ' + str((init_i, init_j)) + ' --> ' + str((cur_i, cur_j)))
        else:
            move = 'right'
            print('Moved LEFT  from ' + str((init_i, init_j)) + ' --> ' + str((cur_i, cur_j)))

        print('Score using ' + evalf + ' heuristic is ' + str(node.get_score() - node.get_level()) + ' in level ' + str(node.get_level()))


        for i in range(goal_state.shape[0]):
            print(np.array(node.get_state()).reshape(goal_state.shape[0], goal_state.shape[0])[i, :]) #Reshape the node game state from list to numpy ndarray
        print()
        time.sleep(1)

        # *******************pass the move variable to the pGUI fns to draw the sliding animation*****************

        slideAnimation(prevState.transpose(), move, animationSpeed=int(TILESIZE/3))

        init_i, init_j = cur_i, cur_j
        prevState = np.array(node.get_state()).reshape(goal_state.shape[0], goal_state.shape[0])


    print(solver.get_summary())

def main(argv):
    max_iter  = 5000000
    heuristic = "manhattan"
    algorithm = "a_star"
    n = 3

    try:
        opts, args = getopt.getopt(argv,"hn:",["mx=", "heur=", "astar", "bfs", "ucs", "gbfs", "dfs",])
    except getopt.GetoptError:
        print('python sliding_puzzle.py -h <help> -n <matrix shape ex: n = 3 -> 3x3 matrix> --mx <maximum_nodes> --heur <heuristic> --astar (default algorithm) or --bfs or --dfs or --gbfs or --ucs')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('python sliding_puzzle.py -h <help> -n <matrix shape ex: n = 3 -> 3x3 matrix> --mx <maximum_nodes> --heur <heuristic> --astar (default algorithm) or --bfs or --dfs or --gbfs or --ucs')
            sys.exit()
        elif opt == '-n':
            n = int(arg)
        elif opt in ("--mx"):
            max_iter = int(arg)
        elif opt in ("--heur"):
            if arg == "manhattan" or arg == "misplaced_tiles":
                heuristic = (arg)
        elif opt in ("--astar"):
            algorithm = "a_star"
        elif opt in ("--bfs"):
            algorithm = "BFS"
        elif opt in ("--ucs"):
            algorithm = "UCS"
        elif opt in ("--dfs"):
            algorithm = "DFS"
        elif opt in ("--gbfs"):
            algorithm = "GBFS"

    while True:
        try:
            init_state = input("Enter a list of " + str(n * n) + " numbers representing the inital state, SEPERATED by WHITE SPACE(1 2 3 etc.): ")
            init_state = init_state.split()
            for i in range(len(init_state)):
                init_state[i] = int(init_state[i])
            goal_state = input("Enter a list of " + str(n * n) + " numbers representing the goal state, SEPERATED by WHITE SPACE(1 2 3 etc.): ")
            goal_state = goal_state.split()
            for i in range(len(goal_state)):
                goal_state[i] = int(goal_state[i])
            if len(goal_state) == len(init_state) and len(goal_state) == n * n:
                break
            else:
                print("Please re-enter the input again correctly")
        except Exception as ex:
            print(ex)

    init_state = np.array(init_state).reshape(n, n)
    goal_state = np.array(goal_state).reshape(n, n)

    if algorithm == "DFS":
        start(init_state, goal_state, max_iter, "no_heuristic", "DFS")
    elif algorithm == "BFS":
        start(init_state, goal_state, max_iter, "no_heuristic", "BFS")
    elif algorithm == "UCS":
        start(init_state, goal_state, max_iter, "cost", "UCS")
    elif algorithm == "GBFS":
        start(init_state, goal_state, max_iter, "greedy", "GBFS")
    else:
        start(init_state, goal_state, max_iter, "manhattan", "A_star")

if __name__ == "__main__":
    main(sys.argv[1:])
